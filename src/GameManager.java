/*  23.06.2018, Nikola Bunis

This is the GameManager app project from 22.06.2018, Telerik Academy Alpha Java+Android.

The goal is to practice inheritance concepts:

Classes and subclasses, overriding and overloading, polymorphism, casting, super and instanceof.

The task is to create a class called "Person", a sub-class of "Person" called "Hero" and another two sub-classes of
"Hero", called "Superhero" and "Supervillain".

Check what super heroes and super villains have in common and classify it within the parent classes, so when creating
the third and final tier of child classes, you would only deal with their differences.

Thus writing similar or even identical code more than once can be avoided. E.g.:

Both heroes and villains have names, weight and gender, so we can put those in the Person class. We can initialise other
objects from that class, that do not have special powers, such as civilians, normal people or
whatever we want to eventually call them.

Next, heroes and villains can share a few more traits, but those should remain unavailable for non-super objects. So in
sub-class Hero we'll put in attributes such as a secret identity, a back story and an moral standing - a boolean field
called simply "isGood", so we can set it to true or false for hero and villain respectively.

We can also agree that both heroes and villains should have super powers, so in the Hero class
we will outline an "addPower" method for both heroes and villains. That can also be extended to some usages of said power,
like attacking and that can again be shared by both sub-classes of hero.

Finally, when building Superhero and Supervillain as classes that inherit attributes and behaviours from the Person and
Hero classes, we'll differentiate them by adding a behaviour method "saveWorld" for heroes and "destroyWorld" for
villains.

Additional behaviours can be invented for practicing overriding and overloading...


 */

import participants.Gender;
import participants.Person;
import participants.SuperPower;
import participants.Superhero;

public class GameManager {

    public static void main (String[] args){


      Superhero Batman = new Superhero("Batman", 95, Gender.MALE, "Bruce Wayne", "Family tragedy",
              true, "intelligence");


    }
}
