/*26.06.2018, Nikola Bunis

This abstract class is created in the main hierarchy, above the Person class.

 It's only purpose is to test the abstract class functionality with a simple method called "be".
 When overridden in the Person class, this method simply prints out that the object "is a living thing".

 */

package participants;

public abstract class LivingOrganisms {


    abstract void be();

}
