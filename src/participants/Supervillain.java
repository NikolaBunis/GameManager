/*
25.06.2018

Class Supervillain outlines "the bad guys", it is fairly poor, as most of the attributes and behaviours
have been outlined in it's parent classes - Person and Hero. The main purpose of class Supervillain and its counterpart -
class Superhero, is to exercise inheritance - they share a lot of traits, but have significant differences or one
significant difference for now - the methods "saveWorld()" and "destroyWorld()". They can be expanded upon with
later ideas, but for now they will contain a simple constructor to inherit attributes from the parent classes and one
method each to differentiate them from one-another.
 */

package participants;

public class Supervillain extends Hero {




    public Supervillain (String name, int weight, Gender gender, int lifePoints, String secretIdentity, String backStory,
                     Alignment alignment) {

        super(name, weight, gender, lifePoints, secretIdentity, backStory, alignment);

    }

    //placeholder message, functionality to be added later
    public void destroyWorld(){

        System.out.println(getName() + "destroyed the WORLD!");


    }






}
