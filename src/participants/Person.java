/* 23.06.2018, Nikola Bunis

A parent class for heroes of both types - super heroes and super villains.

This class will outline the following attributes: name, weight, gender.

It will not be an abstract class or interface, due to the need to instance it at some point, e.g.:
when civilians need to be added to the game as passive participants, but still part of the environment.

For testing purposes, this class will have a default constructor and a constructor for all three attributes,
so the "super" keyword and method overriding can be tested in all sub-classes.

************************************************************************************************

24.06.2018, Nikola Bunis

Adding a life points attribute with limits 0 < N < 100. It will come in handy when the "attack" method is created for the
Hero sub-class. The attribute is added to the Person class, as civilians might end up under attack as well, especially
from villains.

*************************************************************************************************

26.06.2018, Nikola Bunis
Added the move() method via the "Movable" interface, now implemented by the entire Person class.

Added the be() method from the newly created abstract class "LivingOrganisms". This one has no real purpose
except to test abstract classes.

 */



package participants;

public class Person
        extends LivingOrganisms
        implements Movable
        {

    //constants with descriptive names for better validation syntax in setter methods.
    private final int MIN_NAME_LENGTH = 3;
    private final int MAX_NAME_LENGTH = 60;
    private final int MIN_WEIGHT = 30;
    private final int MAX_WEIGHT = 350;
    private final int MIN_LIFE_POINTS = 1;
    private final int MAX_LIFE_POINTS = 100;

    // name field
    private String name;
    // weight field
    private int weight;
    // gender field with a special "Gender" enum created to outline MALE, FEMALE and OTHER as possible values.
    private Gender gender;
     //life points field, used in the "attack" method of the Hero class
    private int lifePoints;


//Note from class: if you have a specific constructor, you have to specify a default constructor as well
    public Person (){

    }
// as noted, this constructor will have multiple purposes, including initialisation of more specific "civillian" objects from the Person class
    public Person(String name, int weight, int lifePoints, Gender gender) {
        setName(name);
        setWeight(weight);
        setLifePoints(lifePoints);
        setGender(gender);
    }


       //name getter (it's debatable if you need this type of comment or not, but I'll leave them in for now :))
    public String getName() {
        return name;
    }
//name setter with validation fo minimum length of 3 and maximum length of 60,
//a placeholder message is printed instead of an exception or any other action
    public void setName(String name) {

        if(name.length() < MIN_NAME_LENGTH || name.length() > MAX_NAME_LENGTH){

            System.out.println("Names can be no shorter than 3 and no longer than 60 characters");
               return;
        }

        this.name = name;

    }

    // weight getter :D
    public int getWeight() {
        return weight;
    }

    //weight setter with validation for minimum value of 30 and maximum value of 350,
//a placeholder message is printed instead of an exception or any other action
    public void setWeight(int weight) {

        if (weight < MIN_WEIGHT || weight > MAX_WEIGHT){

            System.out.println("Weight limit for participants: 30kg. lower; 350 kg. upper");
            return;
        }

        this.weight = weight;
    }

    // gender getter
    public Gender getGender() {
        return gender;
    }
    // gender setter with no actual validation, but it's still good practice to have one and use it in constructors
    // and some such usages... or simply keep it about in case you need to add validation later...
    // I've no experience with any of this, but I'm trying to sound authoritative in the comments :)
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    //life points getter
    public int getLifePoints() {
        return lifePoints;
    }
    //life points setter with validation for minimum value of 1 and maximum value of 100,
//a placeholder message is printed instead of an exception or any other action

    public void setLifePoints(int lifePoints) {

        if (lifePoints < MIN_LIFE_POINTS || lifePoints > MAX_LIFE_POINTS) {

            System.out.println("Life points can be no fewer than 1 and no more than 100");
            return;
        }

        this.lifePoints = lifePoints;
    }
//override of the move method from interface Movable
    @Override
    public void move() {
        System.out.println(getName() + " moves about.");
    }

            @Override
            void be() {
                System.out.println(getName() + " is a living thing!");
            }
        }
