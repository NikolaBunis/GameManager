/* 24.06.2018, Nikola Bunis

Alignment enum for the Hero class, where Superhero objects would usually be GOOD and Supervillain objects would usually be EVIL,
this enum allows for more complex characters to be added and initialised by the parent class if need be.

There is a possibility the alignment is moved to the Person class, so all civilians can be neutral, for example.

*/
package participants;

public enum Alignment {

    GOOD, NEUTRAL, EVIL


}
