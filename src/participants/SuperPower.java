/*25.06.2018, Nikola Bunis

Addition of missing motes from 23.06.2018:

The SuperPower class comes into play when using the "addPower" to hero objects and in the logic of
the "attack" method of those objects.

It has two attributes that are very important for those - name and type. Types are power types,
outlined in an enum. They serve as SuperPower types and immunities for heroes, if a hero is attacked with a power
that has a PowerType that the hero is immune to, no life points are taken away.

 */


package participants;

public class SuperPower {
//constants for clarity when bulding the name setter's  validation
    private final int MIN_NAME_LENGTH = 3;
    private final int MAX_NAME_LENGTH = 60;


    //class fields, the name is a simple string that would get validated, the type is of class/enum PowerType per the initial description
    private String name;
    private PowerType type;

//it's imperative to specify a name and especially a power type for each power when instancing it
    public SuperPower(String name, PowerType type) {

        setName(name);
        setType(type);
    }



    //name getter
    public String getName() {
        return name;
    }
    //name setter with validation fo minimum length of 3 and maximum length of 60,
//a placeholder message is printed instead of an exception or any other action
    public void setName(String name) {

        if (MIN_NAME_LENGTH > name.length() || name.length() > MAX_NAME_LENGTH){

            System.out.println("Super power names can be no shorter than 3 and no longer than 60 characters.");
             return;
        }

        this.name = name;
    }
//type getter
    public PowerType getType() {
        return type;
    }
//type setter, validation is handled via an enum - PowerType
    public void setType(PowerType type) {
        this.type = type;
    }
}
