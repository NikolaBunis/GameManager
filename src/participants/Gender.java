/* 23.06.2018, Nikola Bunis

Gender enum for convenience and clarity when outlining the "gender" attribute for all instances of the "Person" class.

 */

package participants;

public enum Gender {

    FEMALE, MALE, OTHER

}
