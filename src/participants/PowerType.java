/* 23.06.2018, Nikola Bunis

Power types to be used for both Hero immunities and SuperPower types in various behaviours that outline
interactions between Hero objects.

The list may be extended, but for now they are as follows:

Magic, Chemical, Intellect, Tech, Other

 */

package participants;

public enum PowerType {


    MAGIC, CHEMICAL, INTELLECT, TECH, OTHER


}
