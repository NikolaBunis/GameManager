/*26.06.2018, Nikola Bunis

This interface's purpose is more to test the class-interface interaction, but it also might come in handy when
depicting which objects can move.

It's initial implementation is in the Person class, where Person overrides the move() method, so all instances
of Person can "move about".

 */

package participants;

public interface Movable {


    void move();


}
