/*25.06.2018

Class Superhero outlines "the good guys", it is fairly poor, as most of the attributes and behaviours
have been outlined in it's parent classes - Person and Hero. The main purpose of class Superhero and its counterpart -
class Supervillain, is to exercise inheritance - they share a lot of traits, but have significant differences or one
significant difference for now - the methods "saveWorld()" and "destroyWorld()". They can be expanded upon with
later ideas, but for now they will contain a simple constructor to inherit attributes from the parent classes and one
method each to differentiate them from one-another.

 */

package participants;

public class Superhero extends Hero {


    public Superhero(String name, int weight, Gender gender, int lifePoints, String secretIdentity, String backStory,
                     Alignment alignment) {

        super(name, weight, gender, lifePoints, secretIdentity, backStory, alignment);

    }

    //placeholder message, functionality to be added later
    public void saveWorld(){

        System.out.println(getName() + "saved the WORLD!");

    }





}
