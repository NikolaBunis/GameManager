/* 23.06.2018, Nikola Bunis

Class Hero extends Person and adds more attributes and actual behaviours,
that will be valid for both heroes and villains.

Attributes:
- secret identity

- back story

- a boolean variable to determine if the participant is good (true) or bad(false)

- a list of powers: a list of super powers, initially the concept was to have an enum with a few powers, but
I decided to continue the practice from a previous project and create a class SuperPower with attributes name and type
where the name will be used to describe it in a simple fashion and the type will be used to for extra colour to the
concept and more importantly - to create the immunities attribute for each Hero object.

- a list of immunities: these are power types outlined in an enum to help with both the immunities concept and the power type concept.
To be used during interactions between participants later on.

Behaviours:
- add super power: this would be a function to add a super power
- add immunity
- "attack" method, borrowed from previous exercises - interaction between Hero objects will apply attacks
based on objects from the super power class and will reduce hero life points or do nothing depending on the SuperPower
type and the Hero immunity

********************************************************

24.06.2018, Nikola Bunis

Finishing powers and immunities getters and setters setter.

Adding the attach method, which will utilise the Person attribute lifePoints, where heroes would be able to attack
each other as well as civilians.

Amending the main Hero Constructor to reflect the lifePoints attribute added to the Person class.

Creating descriptive comments.

Expanding Hero class constructors to reflect the Lift of Powers attribute and the List of Immunities

Changing the isGood boolean to an enum names Alignment, to expand the concept to GOOD, NEUTRAL and EVIL, for now.
Later on more complex alignments can be added as well, like the D&D ones - chaotic good, lawful neutral, etc. if
the project expands.

 */


package participants;

import java.util.LinkedList;

// child class of Person. While the main classes to use are Hero's two sub-classes: Superhero and Supervillain,
// instances of both Hero and Person will be needed to complete some basic concepts for this project.
public class Hero extends Person {


    //constant values for the boundaries of each hero's Secret identity and Back story.
    private final int MIN_SECRET_IDENTITY_LENGTH = 3;
    private final int MAX_SECRET_IDENTITY_LENGTH = 60;
    private final int MIN_BACK_STORY_LENGTH = 10;
    private final int MAX_BACK_STORY_LENGTH = 150;

    //shared attributes for both heroes and villains
    private String secretIdentity;

    private String backStory;

    private Alignment alignment;

    private LinkedList<SuperPower> powers;

    private LinkedList<PowerType> immunities;


    //main constructor - utilises the "super" keyword to make use of the Person class' main constructor.
    public Hero(String name, int weight, int lifePoints, Gender gender, String secretIdentity, String backStory, Alignment alignment) {

        super(name, weight, lifePoints, gender);
        setSecretIdentity(secretIdentity);
        setBackStory(backStory);
        setAlignment(alignment);

    }

    //extended constructor that includes powers and immunities, will come in handy when there are ready made lists to be added
    public Hero(String name, int weight, int lifePoints, Gender gender, String secretIdentity, String backStory,
                Alignment alignment, LinkedList<SuperPower> powers, LinkedList<PowerType> immunities) {

        this(name, weight, lifePoints, gender, secretIdentity, backStory, alignment);
        setPowers(powers);
        setImmunities(immunities);


    }

    //default constructor per the approach in this project

    public Hero(String name, int weight, Gender gender, int lifePoints, String secretIdentity, String backStory, Alignment alignment) {

    }

    //secret identity getter
    public String getSecretIdentity() {
        return secretIdentity;
    }


    //secret identity setter with validation fo minimum length of 3 and maximum length of 60,
    ////a placeholder message is printed instead of an exception or any other action
    public void setSecretIdentity(String secretIdentity) {

        if (secretIdentity.length() < MIN_SECRET_IDENTITY_LENGTH || secretIdentity.length() > MAX_SECRET_IDENTITY_LENGTH) {
            System.out.println("Secret identities can be no shorter than 3 and no longer than 60 characters");
            return;
        }
        this.secretIdentity = secretIdentity;
    }

    //back story getter
    public String getBackStory() {
        return backStory;
    }

    //back story setter with validation fo minimum length of 10 and maximum length of 150,
    ////a placeholder message is printed instead of an exception or any other action
    public void setBackStory(String backStory) {

        if (backStory.length() < MIN_BACK_STORY_LENGTH || backStory.length() > MAX_BACK_STORY_LENGTH) {
            System.out.println("Back story size can be no smaller than 10 characters and no larger than 150");
            return;

        }

        this.backStory = backStory;
    }

    //super powers getter
    public LinkedList<SuperPower> getPowers() {
        return powers;
    }

    //this is a method to add a single super power to a hero
    public void addPower(SuperPower power) {
        getPowers().add(power);
    }

    //setter for a whole list of powers, will come in handy with pre-defined lists
    public void setPowers(LinkedList<SuperPower> powers) {

        this.getPowers().addAll(powers);

    }

    //immunities getter a for-loop included as a comment in case copy-paste is needed :)
    public LinkedList<PowerType> getImmunities() {

        //the for loop to print the immunities list

        /*for (int i = 0; i < immunities.size(); i++) {

            System.out.println(immunities.get(i));

        }*/
        return immunities;
    }

    //a method to add a single immunity to to a hero
    public void addImmunity(PowerType immunity) {

        getImmunities().add(immunity);


    }

    //setter for a whole list of immunities, will come in handy with pre-defined lists
    public void setImmunities(LinkedList<PowerType> immunitiesList) {

        getImmunities().addAll(immunitiesList);

    }

    //alignment getter
    public Alignment getAlignment() {
        return alignment;
    }

    //alignment setter, validation handled with and Alignment enum -  GOOD, EVIL, NEUTRAL

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }


    /*
    This method outlines the "attack" behaviour of heroes towards persons. The check for immunity has a downcast
    to the Hero sub-class, as Person objects cannot have immunities(this attribute is in the Hero class).
    There is a placeholder message for if the attacked hero is immune to the power's power type, the text has been
    built to include all object and variable names - attacker, power used, power type, attacked.
    */

    public void attack(Hero attacker, SuperPower power, Person attacked) {

        if(attacked instanceof Hero && ((Hero)attacked).getImmunities().contains(power.getType())){

            System.out.println(attacked.getName() + "is immune to " + attacker.getName()+ "'s"
                    + power.getName() + "'s power type(" + power.getType() + ")");

            return;

            }

            attacked.setLifePoints(getLifePoints() - 10);

        }

    }
